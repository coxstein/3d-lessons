﻿using System;
using System.Collections;
using UnityEngine;

public enum CharacterState
{
    Idle,
    Move,
    Attack,
    Skill,
    Hit,
    Dead,
}

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    private CharacterState characterState;
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject ballPrefab;
    [SerializeField] private Transform firePoint;
    [SerializeField] private int skillBallsCount;
    [SerializeField] private float skillAngleBound = 45f;

    private static readonly int AttackTrigger = Animator.StringToHash("Attack");
    private static readonly int SkillTrigger = Animator.StringToHash("Skill");

    public void AttackEvent()
    {
        var obj = Instantiate(ballPrefab, firePoint.transform.position, Quaternion.identity);
        var rig = obj.GetComponent<Rigidbody>();
        if (rig)
        {
            rig.AddForce(Vector3.forward * 5f, ForceMode.Impulse);
        }
    }

    public void SkillEvent()
    {
        var angleStep = skillAngleBound * 2 / skillBallsCount - 1;
        for (int i = 0; i < skillBallsCount; i++)
        {
            var y = skillAngleBound - i * angleStep;
            var rotation = Quaternion.Euler(0f, y, 0f);
            var obj = Instantiate(ballPrefab, firePoint.transform.position, rotation);
            
            obj.transform.Translate(obj.transform.forward * 0.3f);
            var rig = obj.GetComponent<Rigidbody>();
            if (rig)
            {
                rig.AddForce(obj.transform.forward * 5f, ForceMode.Impulse);
            }
        }
    }

    private void Reset()
    {
        animator = GetComponent<Animator>();
    }

    void Start()
    {
        characterState = CharacterState.Move;
        InputController.OnInputAction += OnInputCommand;
        StartCoroutine(MovementProcess(3f));
    }

    private IEnumerator MovementProcess(float time)
    {
        animator.SetInteger("Movement", 1);
        float timer = Time.time + time;
        while (Time.time < timer)
        {
            transform.Translate(transform.forward * Time.deltaTime);
            yield return null;
        }

        characterState = CharacterState.Idle;
        animator.SetInteger("Movement", 0);
    }

    private void OnDestroy()
    {
        InputController.OnInputAction -= OnInputCommand;
    }

    private void OnInputCommand(InputCommand inputCommand)
    {
        switch (inputCommand)
        {
            case InputCommand.Fire:
                Attack();
                break;
            case InputCommand.Skill:
                Skill();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(inputCommand), inputCommand, null);
        }
    }

    private void Attack()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }

        animator.SetTrigger(AttackTrigger);
        characterState = CharacterState.Attack;
        DelayRun.Execute(delegate { characterState = CharacterState.Idle; }, 0.5f, gameObject);
    }

    private void Skill()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }

        animator.SetTrigger(SkillTrigger);
        characterState = CharacterState.Skill;
        DelayRun.Execute(delegate { characterState = CharacterState.Idle; }, 1f, gameObject);
    }
}